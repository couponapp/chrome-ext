export default class Storage {

    constructor(key) {
        this.key = key;
    }

    removeFromStorage() {
        let self = this;
        return new Promise((resolve, reject) => {
            chrome.storage.local.remove(this.key, function() {
                //console.info('Data removed from storage from key - ', self.key)
                resolve(true);
            });
        });
    }

    getFromStorage() {
        let self = this;
        return new Promise((resolve, reject) => {
            chrome.storage.local.get([this.key], function(result) {
                //console.info('Data fetch from storage from key - ', self.key, ' Data is ', result[self.key])
                resolve(result[self.key]);
            });
        });
    }

    saveToStorage(storeData) {
        let self = this;
        return new Promise((resolve, reject) => {
            chrome.storage.local.set({ [this.key] : storeData }, function() {
                //console.info('Data save to storage by key - ', self.key, ' Data is ', storeData)
                resolve(true);
            });
        });
    }

}