import * as types from './mutation-types'

let CLICKED = false;
let TEMP;

export default {
  [types.UPDATE_FULL_FEED] (state, payload) {
    state.feed = payload
  },
  [types.UPDATE_SINGLE_STORE_FEED] (state, payload) {
    state.storeFeed = payload
  },
  [types.UPDATE_STORE_NAME_LIST] (state, payload) {
    state.storeList = payload
  },
  [types.UPDATE_SINGLE_STORE_OFFER_COUNT] (state, payload) {
    state.offerCount = payload
  },
  [types.FILTER_OFFERS] (state, { value }) {

    if ( !CLICKED ) {

      TEMP = state.storeFeed.offers
      CLICKED = true

    }

    if ( !value ) {
      //console.log("unchecked checkbox", TEMP, state.storeFeed.offers)
      state.storeFeed.offers = TEMP.filter( item => item.type === 'Code' )
    } else {
      //console.log("checked checkbox", TEMP, state.storeFeed.offers)
      state.storeFeed.offers = TEMP
    }

  },
  [types.UPDATE_MOMENT] (state, payload) {
    state.moment = payload
  }
}
