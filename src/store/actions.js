import * as types from './mutation-types'
import Storage from './storage'
import { apolloClient } from '../popup/plugin/graphql_apollo'
import { GET_ALL_FEED } from '../popup/gql/allFeed'

/**
 * Fetch feed from external api
 * Save to feed and storeList variables
 */
export const getFullFeed = async function({commit}) {
  try {

    const response = await fetchFeed(3)
    const { stores } = response.data

    if ( !Array.isArray(stores) || !stores.length  ) return

    //console.info('Fetch stores from api - ', stores) //DEBUG
  
    const names = returnStoresName(stores)

    const storageFeed = new Storage('feed')
    await storageFeed.saveToStorage(stores)

    const storeNames  = new Storage('store_names')
    await storeNames.saveToStorage(names)
  
    commit(types.UPDATE_FULL_FEED, stores)
    commit(types.UPDATE_STORE_NAME_LIST, names)

  } catch (error) {
    console.error(`Error at fetching feed from api ${error.message}`)
  }
}

/**
 * Load store name list from storage
 */
export const getStoreListFromStorage = async function({commit}) {
  const storage = new Storage('store_names')
  const names = await storage.getFromStorage()

  commit(types.UPDATE_STORE_NAME_LIST, names)
}

/**
 * Get certant store from feed array
 * Used in popup
 * @param {*} hostName 
 */
export const getStoreFeedFromStorage = async function({commit}, hostName) {
  try {

    const storage = new Storage('feed')
    const feed = await storage.getFromStorage()
  
    const names = returnStoresName(feed)
  
    const { success, host } = checkHostname(names, hostName)
  
    if ( !success ) return;
    
    const index = feed.findIndex( store => store.name === host )
  
    commit(types.UPDATE_SINGLE_STORE_FEED, feed[index]) 

  } catch (error) {
    console.error(`Error at get single store from feed ${error.message}`)
  }
}

/**
 * Count numbers of offers of certan store
 * Used in background script, show total numer of offers in popup icon 
 * @param {*} hostname 
 */
export const updateStoreOfferCount = async function({commit}, hostname) {
  try {
    
    const storage = new Storage('feed')
    const feed    = await storage.getFromStorage()
  
    const index = feed.findIndex( store => store.name === hostname )
  
    const count = (feed[index].offers).length
  
    commit(types.UPDATE_SINGLE_STORE_OFFER_COUNT, count) 

  } catch (error) {
    console.error(`Error at update offers count ${error.message}`)
  }
}

/**
 * Update moment
 * Used in background script
 * @param {*} hostname 
 */
export const updateStoreClick = async function({commit}, hostname) {
  try {
    const feedStorage = new Storage('feed')
    const feed        = await feedStorage.getFromStorage()

    // This is dirty trick
    const storeNamesStorage   = new Storage('store_names')
    const storeNames          = await storeNamesStorage.getFromStorage()
    const { _, host } = checkHostname(storeNames, hostname)
    // end of hack

    const index = feed.findIndex( store => store.name === host )

    const link = feed[index].deepLink
  
    const momentStorage = new Storage(host)
    const moment        = await momentStorage.getFromStorage()
  
    if ( !moment ) {
      await momentStorage.saveToStorage({
        time: Math.floor(new Date().getTime()/1000),
        link: link,
      })
    } else {
      await momentStorage.saveToStorage({
        time: moment.time,
        link: link,
      });
    }

    commit(types.UPDATE_MOMENT, moment)

  } catch (error) {
    console.error(`Error at update store click ${error.message}`)
  }
}

export const updateStoreClickStorage = async function({_}, hostname) {

  // One more dirty trick
  const storeNamesStorage   = new Storage('store_names')
  const storeNames          = await storeNamesStorage.getFromStorage()
  const { success, host } = checkHostname(storeNames, hostname)
  // end of hack

  const momentStorage = new Storage(host)
  const moment        = await momentStorage.getFromStorage()

  await momentStorage.saveToStorage({
    time: Math.floor(new Date().getTime()/1000),
    link: moment.link,
  })

}

/**
 * HELPER FUNCTION BLOCK
*/
const fetchFeed = async (attempts) => {
  try {
    return await apolloClient.query({
      query:GET_ALL_FEED
    })
  } catch (err) {
    if ( attempts === 1 ) throw err;
    return await fetchFeed(attempts - 1)
  }
}

const returnStoresName = (arr) => {
  return arr.map( store => store.name )
}

const checkHostname = (storesFromStorage, currentHostname) => {
  for ( let store of storesFromStorage ) {
      if ( currentHostname.indexOf(store.toLowerCase()) > -1 ) {
        return { success:true, host:store }
      }
  }
  return { success:false, host:'' }
}
