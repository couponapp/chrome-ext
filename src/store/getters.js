export const getAllFeed         = (state) => state.feed
export const getStoreFeed       = (state) => state.storeFeed
export const getStoreList       = (state) => state.storeList
export const getStoreFeedCount  = (state) => state.offerCount
export const getMoment          = (state) => state.moment