global.browser = require('webextension-polyfill')
import store from './store'

function BG() {

    var self = this;

    this.STORE_LIST;
    this.FETCH_FEED = 120;
    this.FETCH_FEED_FIRST = 1;

    __init();

    async function __init() {
        chrome.runtime.setUninstallURL('https://spot.shopping/uninstall');
        chrome.runtime.onInstalled.addListener(installerListner);
        chrome.tabs.onUpdated.addListener(tabUpdated);
        chrome.alarms.onAlarm.addListener(alarmListner);
        chrome.alarms.create('fetch-feed', {
            delayInMinutes: self.FETCH_FEED_FIRST,
            periodInMinutes: self.FETCH_FEED
        });
        await store.dispatch('getStoreListFromStorage');
        self.STORE_LIST = store.getters.getStoreList;
    }

    async function tabUpdated(tabId, changeInfo, tab) {

        const tabUrl = tab.url;

        if ( changeInfo.status !== 'complete' || tabUrl.startsWith('chrome://')) return;
        
        const hostnameTrim = new URL(String(tabUrl)).hostname;
        
        const { success, host } = checkHostname(hostnameTrim)
        
        if ( !success ) return;
        
        //await store.dispatch('updateStoreClick', host);

        await store.dispatch('updateStoreOfferCount', host);
        const count = store.getters.getStoreFeedCount;

        chrome.browserAction.setIcon({ path:'/icons/128-color.png', tabId });
        chrome.browserAction.setBadgeText({text: String(count), tabId});
        chrome.browserAction.setBadgeBackgroundColor({color:"#F13451", tabId})

    }

    function checkHostname(trimUrl) {
        if ( !Array.isArray(self.STORE_LIST) || !(self.STORE_LIST).length ) {
            //console.info('When i was checked hostname array of stores was empty');
            return { success: false, host: ''};
        }
        for ( let store of self.STORE_LIST ) {
            if ( trimUrl.indexOf(store.toLowerCase()) > -1 ) { 
                return { success: true, host: store };
            }
        }

        return { success: false, host: ''};
    }

    async function alarmListner(alarm) {
        switch (alarm.name) {
            case 'fetch-feed':
                console.info('Update fired', Math.floor(new Date().getTime()/1000));
                await store.dispatch('getFullFeed');
                self.STORE_LIST = store.getters.getStoreList;
                break;
            default:
                break;
        }
    }

    async function installerListner() {
        await store.dispatch('getFullFeed');
        self.STORE_LIST = store.getters.getStoreList;
    }

}

new BG()