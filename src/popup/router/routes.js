import Coupons from './pages/Coupons'
import Stores from './pages/Stores'
import Store from './pages/Store'

export default [
  {
    path: '/',
    component: Coupons
  },
  {
    path: '/stores',
    component: Stores
  },
  {
    path: '/stores/:name',
    name: 'store',
    component: Store
  }
]
