import gql from 'graphql-tag';

export const GET_ALL_FEED = gql`
    query Stores {
        stores {
            image
            name
            deepLink
            offers {
                offerUnique
                offerID
                offer
                title
                description
                type
                code
                affiliateLink
                link
                status
                rating
                endDate
                startDate
            }
        }
    }
`