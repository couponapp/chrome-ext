import gql from 'graphql-tag';

export const GET_ALL_COUPONS = gql`
    query Store($name: String!) {
        store(name: $name) {
            name
            image
            deepLink
            offers {
                offerUnique
                offerID
                offer
                title
                description
                type
                code
                imageLink
                affiliateLink
                link
                status
                rating
                endDate
                startDate
            }
        }
    }
`