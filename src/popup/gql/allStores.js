import gql from 'graphql-tag';

export const GET_ALL_STORES = gql`
    query Stores {
        stores {
            name
        }
    }
`