import Vue from 'vue'
import App from './App'
import store from '../store'
import router from './router'
import VueApollo from 'vue-apollo'
import InstantSearch from 'vue-instantsearch'
import { apolloClient } from './plugin/graphql_apollo'

global.browser = require('webextension-polyfill')
Vue.prototype.$browser = global.browser

Vue.use(InstantSearch)
Vue.use(VueApollo)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

new Vue({
  el: '#app',
  store,
  router,
  apolloProvider,
  render: h => h(App)
})
