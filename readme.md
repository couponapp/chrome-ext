## OS VERSION and INFO

OS - Windows 10. Version 1903 (OS Build 18362.535)
NPM - 6.12.0
NODE - 12.13.0

## HOW TO BUILD

1. Open console in this folder
2. Run ```npm install```
3. Run ```npm run build```
4. The builded extension will be in the dist folder
5. Open manifest.json in dist folder and add ```"content_security_policy":"script-src 'self' 'unsafe-eval'; object-src 'self'"```
6. Done